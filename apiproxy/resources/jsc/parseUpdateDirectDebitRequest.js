var request = context.getVariable("request.content");

try {
    var requestObj = JSON.parse(request);
    
    context.setVariable("customerNumber", requestObj.billingId); //parse customer/product info needed for CRS request
    context.setVariable("productType", requestObj.product.type);
    context.setVariable("productDateEffective", requestObj.product.dateEffective);
    context.setVariable("targetBankAccount", requestObj.product.targetBankAccount);
    
    context.setVariable("bankAccountType", requestObj.bankAccount.type); //parse bankAcct/payment info needed for CRS request
    context.setVariable("bankAccountName", requestObj.bankAccount.nickname);
    context.setVariable("bankAccountNumber", requestObj.bankAccount.number); 
    context.setVariable("accountAction", requestObj.bankAccount.action);
    context.setVariable("paymentCode", requestObj.bankAccount.paymentCode);
    context.setVariable("bankAccountDateEffective", requestObj.bankAccount.dateEffective);
}
catch (err) {
    print("JSON parsing error: ", err);
}

