 var response = context.getVariable("xmlResponse.SABbody");
var responseCode = context.getVariable("xmlResponse.responseCode");
var asOfDateTime = context.getVariable("xmlResponse.asOfDateTime");

if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    var customerNumber = responseBody.customerNumber;
    var directDebitArrangement = responseBody.deletedDirectDebitArrangement;

    
    var responseObject = {};
    var finalObject = {};
   
     if (typeof directDebitArrangement != 'undefined'){
         
        finalObject.billingId = directDebitArrangement.customerNumber.toString();
        var flag=0;
          
          if(directDebitArrangement.manualDirectDebit) {
              flag =1;
              var manualPayProduct = {};
              manualPayProduct.type = directDebitArrangement.manualDirectDebit.arrangementType;
              manualPayProduct.description = directDebitArrangement.manualDirectDebit.typeDescription;
              manualPayProduct.dateEffective = directDebitArrangement.dateEffective;
              manualPayProduct.targetBankAccount = directDebitArrangement.targetBankAccountNumber;
          
              finalObject.manualPayProduct = manualPayProduct;
              

          }else if(directDebitArrangement.directCredit) {
              flag=2;
              var directCreditProduct = {};
              directCreditProduct.type = directDebitArrangement.directCredit.arrangementType;
              directCreditProduct.description = directDebitArrangement.directCredit.typeDescription;
              directCreditProduct.dateEffective = directDebitArrangement.dateEffective;
              directCreditProduct.targetBankAccount = directDebitArrangement.targetBankAccountNumber;
          
               finalObject.directCreditProduct = directCreditProduct;
                                
              
          }else if (directDebitArrangement.demandDirectDebit){
              flag=3;
              var autoPayProduct = {};
              autoPayProduct.type = directDebitArrangement.demandDirectDebit.arrangementType;
              autoPayProduct.description = directDebitArrangement.demandDirectDebit.typeDescription;
              autoPayProduct.dateEffective = directDebitArrangement.dateEffective;
              autoPayProduct.targetBankAccount = directDebitArrangement.targetBankAccountNumber;
            
                finalObject.autoPayProduct = autoPayProduct;

          }
          
          
            if(directDebitArrangement.pendingDirectDebits && directDebitArrangement.pendingDirectDebits.pendingDirectDebit) {
                 var payments = [];
                 var customerPayments = {};
                 if (!Array.isArray(directDebitArrangement.pendingDirectDebits.pendingDirectDebit)) {
                   customerPayments = Array(directDebitArrangement.pendingDirectDebits.pendingDirectDebit);
                 }else {
                     customerPayments = directDebitArrangement.pendingDirectDebits.pendingDirectDebit;
                 }
                 
                 print("payments length"+ customerPayments.length);

                 for (var j = 0; j < customerPayments.length; j++) {
                    var payment = {};
                    payment.confirmationReceiptId = customerPayments[j].pendingDirectDebitNumber;
                    payment.datePosted = customerPayments[j].scheduledPaymentDate;
                    payment.amount = customerPayments[j].scheduledPaymentAmount;
                    payment.isModifiable = "true";
                    payment.status = "P";
                    payment.statusDescription = "Pending";

                    if(customerPayments[j].directDebitArrangementType === "AUTOMATIC_DIRECT_DEBIT"){
                        payment.method = "DD"
                        payment.methodDescription = "Auto Pay (Demand/DD)"
                    }else if(customerPayments[j].directDebitArrangementType === "MANUAL_DIRECT_DEBIT"){
                        payment.method = "DM"
                        payment.methodDescription = "Phone Pay-Manual/DM"
                    }else if(customerPayments[j].directDebitArrangementType === "DIRECT_CREDIT") {
                        payment.method = "DC"
                        payment.methodDescription = "Direct Credit/DC"
                    }

                    var bankAccount = {};
                    bankAccount.type = customerPayments[j].customerBankAccount.bankAccount.accountType;
                    bankAccount.name = customerPayments[j].customerBankAccount.bankAccount.accountName;
                    bankAccount.fullNumber = customerPayments[j].customerBankAccount.bankAccount.accountNumber;
                    bankAccount.paymentCode = customerPayments[j].customerBankAccount.paymentCode;
                    bankAccount.paymentDescription = customerPayments[j].customerBankAccount.paymentDescription;
                    bankAccount.effectiveDate = customerPayments[j].customerBankAccount.dateEffective;
                    
                    bankAccount.routing = bankAccount.fullNumber.toString().split("-")[0];
                    
                    if(typeof bankAccount.fullNumber.toString().split("-")[1] != 'undefined'){
                    bankAccount.number = bankAccount.fullNumber.toString().split("-")[1];
                    }else{
                        bankAccount.number = "";
                    }
                    bankAccount.id = bankAccount.name + '-' + bankAccount.number;
                    
                    payment.bankAccount = bankAccount;

                    payments.push(payment);
                     
                 }
                 if(flag==1){
                    manualPayProduct.payments= payments;
                 }else if(flag==2){
                    directCreditProduct.payments= payments;
                 }else if(flag==3) {
                     autoPayProduct.payments= payments;
                 }
            }
            
     }else {
         var errorDetail = 'CRS customerNumber: ' + customerNumber + ' not found.';
        context.setVariable("isValidResponse", false);
        context.setVariable("responseMessage", errorDetail);
     }
    context.setVariable("finalResponse", JSON.stringify(finalObject));
}