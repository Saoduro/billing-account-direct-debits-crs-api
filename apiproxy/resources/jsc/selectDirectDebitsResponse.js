var response = context.getVariable("xmlResponse.SABbody");
var responseCode = context.getVariable("xmlResponse.responseCode");
var asOfDateTime = context.getVariable("xmlResponse.asOfDateTime");
if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    //var correlationId = context.getVariable("correlationId");
    var customerNumber = responseBody.customerNumber;
    var directDebitArrangement = responseBody.directDebitArrangements.directDebitArrangement;
    print('loop:arrangements', directDebitArrangement);
    var responseObject = {};
    var finalObject = {};
    if (typeof directDebitArrangement != 'undefined') {
        if (!Array.isArray(directDebitArrangement)) {
            directDebitArrangement = Array(responseBody.directDebitArrangements.directDebitArrangement);
        }
        //finalObject.correlationId = correlationId;
        finalObject.billingId = directDebitArrangement[0].customerNumber.toString();
        for (var i = 0; i < directDebitArrangement.length; i++) {
            print('inside_i_loop'); {
                print('length:', directDebitArrangement.length);
                print('count:', i);
                print(typeof directDebitArrangement[i].automaticDirectDebit);
                print(directDebitArrangement[i].automaticDirectDebit);
                // Do this once, we'll use it for every type
                var customerBankAccount = directDebitArrangement[i].customerBankAccounts.customerBankAccount;
                if (!Array.isArray(customerBankAccount)) {
                    customerBankAccount = Array(directDebitArrangement[i].customerBankAccounts.customerBankAccount);
                }
                if (directDebitArrangement[i].demandDirectDebit) {
                    var autoPayProduct = {};
                    print('I am in the autopay direct debit loop');
                    autoPayProduct.type = directDebitArrangement[i].demandDirectDebit.arrangementType;
                    autoPayProduct.description = directDebitArrangement[i].demandDirectDebit.typeDescription;
                    autoPayProduct.dateEffective = directDebitArrangement[i].dateEffective;
                    autoPayProduct.targetBankAccount = directDebitArrangement[i].targetBankAccountNumber;
                    autoPayProduct.daysDelay = directDebitArrangement[i].demandDirectDebit.daysDelay;
                    autoPayProduct.earlyCreationDays = directDebitArrangement[i].demandDirectDebit.earlyCreationDays;
                    print('Ready for the customer bank account');
                    for (var j = 0; j < customerBankAccount.length; j++) {
                        print('inside_j_loop'); {
                            print('length:', customerBankAccount.length);
                            print('count:', j);
                            print(customerBankAccount[j]);
                            print(customerBankAccount[j]);
                            var bankAccount = {};
                            print('bankAccountType: ', customerBankAccount[j].bankAccount.accountType);
                            bankAccount.type = customerBankAccount[j].bankAccount.accountType;
                            var accountNumber = customerBankAccount[j].bankAccount.accountNumber.toString();
                            bankAccount.fullNumber = accountNumber.toString();
                            var accountName = customerBankAccount[j].bankAccount.accountName;
                            bankAccount.effectiveDate = customerBankAccount[j].dateEffective;
                            bankAccount.paymentCode = customerBankAccount[j].paymentCode;
                            bankAccount.paymentDescription = customerBankAccount[j].paymentDescription;
                            bankAccount.isModifiable = "false";
                            bankAccount.routing = accountNumber.substring(0, 8);
                            bankAccount.number = accountNumber.substring(9);
                            accountNumberLast4 = accountNumber.substring(accountNumber.length - 4);
                            bankAccount.id = accountName + '-' + accountNumberLast4;
                        }
                    }
                    autoPayProduct.bankAccount = bankAccount;
                }
                if (directDebitArrangement[i].manualDirectDebit) {
                    var manualPayProduct = {};
                    print('I am in the manual pay direct debit loop');
                    manualPayProduct.type = directDebitArrangement[i].manualDirectDebit.arrangementType;
                    manualPayProduct.description = directDebitArrangement[i].manualDirectDebit.typeDescription;
                    manualPayProduct.dateEffective = directDebitArrangement[i].dateEffective;
                    manualPayProduct.targetBankAccount = directDebitArrangement[i].targetBankAccountNumber;
                    for (var j = 0; j < customerBankAccount.length; j++) {
                        print('inside_j_loop'); {
                            print('length:', customerBankAccount.length);
                            print('count:', j);
                            print(customerBankAccount[j]);
                            print(customerBankAccount[j]);
                            var bankAccount = {};
                            print('bankAccountType: ', customerBankAccount[j].bankAccount.accountType);
                            bankAccount.type = customerBankAccount[j].bankAccount.accountType;
                            var accountNumber = customerBankAccount[j].bankAccount.accountNumber.toString();
                            bankAccount.fullNumber = accountNumber.toString();
                            var accountName = customerBankAccount[j].bankAccount.accountName;
                            bankAccount.effectiveDate = customerBankAccount[j].dateEffective;
                            bankAccount.paymentCode = customerBankAccount[j].paymentCode;
                            bankAccount.paymentDescription = customerBankAccount[j].paymentDescription;
                            bankAccount.isModifiable = "false";
                            bankAccount.routing = accountNumber.substring(0, 8);
                            bankAccount.number = accountNumber.substring(9);
                            accountNumberLast4 = accountNumber.substring(accountNumber.length - 4);
                            bankAccount.id = accountName + '-' + accountNumberLast4;
                        }
                    }
                    manualPayProduct.bankAccount = bankAccount;
                }
                if (directDebitArrangement[i].directCredit) {
                    var directCreditProduct = {};
                    print('I am in the direct credit loop');
                    directCreditProduct.type = directDebitArrangement[i].directCredit.arrangementType;
                    directCreditProduct.description = directDebitArrangement[i].directCredit.typeDescription;
                    directCreditProduct.dateEffective = directDebitArrangement[i].dateEffective;
                    directCreditProduct.targetBankAccount = directDebitArrangement[i].targetBankAccountNumber;
                    for (var j = 0; j < customerBankAccount.length; j++) {
                        print('inside_j_loop'); {
                            print('length:', customerBankAccount.length);
                            print('count:', j);
                            print(customerBankAccount[j]);
                            print(customerBankAccount[j]);
                            var bankAccount = {};
                            print('bankAccountType: ', customerBankAccount[j].bankAccount.accountType);
                            bankAccount.type = customerBankAccount[j].bankAccount.accountType;
                            var accountNumber = customerBankAccount[j].bankAccount.accountNumber.toString();
                            bankAccount.fullNumber = accountNumber.toString();
                            var accountName = customerBankAccount[j].bankAccount.accountName;
                            bankAccount.effectiveDate = customerBankAccount[j].dateEffective;
                            bankAccount.paymentCode = customerBankAccount[j].paymentCode;
                            bankAccount.paymentDescription = customerBankAccount[j].paymentDescription;
                            bankAccount.isModifiable = "false";
                            bankAccount.routing = accountNumber.substring(0, 8);
                            bankAccount.number = accountNumber.substring(9);
                            accountNumberLast4 = accountNumber.substring(accountNumber.length - 4);
                            bankAccount.id = accountName + '-' + accountNumberLast4;
                        }
                    }
                    directCreditProduct.bankAccount = bankAccount;
                }
                if (directDebitArrangement[i].automaticDirectDebit) {
                    var automaticDirectProduct = {};
                    print('I am in the automatic direct debit loop');
                    automaticDirectProduct.type = directDebitArrangement[i].automaticDirectDebit.arrangementType;
                    automaticDirectProduct.description = directDebitArrangement[i].automaticDirectDebit.typeDescription;
                    automaticDirectProduct.dateEffective = directDebitArrangement[i].dateEffective;
                    automaticDirectProduct.targetBankAccount = directDebitArrangement[i].targetBankAccountNumber;
                    for (var j = 0; j < customerBankAccount.length; j++) {
                        print('inside_j_loop'); {
                            print('length:', customerBankAccount.length);
                            print('count:', j);
                            print(customerBankAccount[j]);
                            print(customerBankAccount[j]);
                            var bankAccount = {};
                            print('bankAccountType: ', customerBankAccount[j].bankAccount.accountType);
                            bankAccount.type = customerBankAccount[j].bankAccount.accountType;
                            var accountNumber = customerBankAccount[j].bankAccount.accountNumber.toString();
                            bankAccount.fullNumber = accountNumber.toString();
                            var accountName = customerBankAccount[j].bankAccount.accountName;
                            bankAccount.effectiveDate = customerBankAccount[j].dateEffective;
                            bankAccount.paymentCode = customerBankAccount[j].paymentCode;
                            bankAccount.paymentDescription = customerBankAccount[j].paymentDescription;
                            bankAccount.isModifiable = "false";
                            bankAccount.routing = accountNumber.substring(0, 8);
                            bankAccount.number = accountNumber.substring(9);
                            accountNumberLast4 = accountNumber.substring(accountNumber.length - 4);
                            bankAccount.id = accountName + '-' + accountNumberLast4;
                        }
                    }
                    automaticDirectProduct.bankAccount = bankAccount;
                }
            }
        }

        finalObject.directCreditProduct = directCreditProduct;
        finalObject.manualPayProduct = manualPayProduct;
        finalObject.autoPayProduct = autoPayProduct;
        finalObject.automaticDirectProduct = automaticDirectProduct;
        responseObject = finalObject;
    } else {
        var errorDetail = 'CRS customerNumber: ' + customerNumber + ' not found.';
        context.setVariable("isValidResponse", false);
        context.setVariable("responseMessage", errorDetail);
    }
    context.setVariable("finalResponse", JSON.stringify(responseObject));
}