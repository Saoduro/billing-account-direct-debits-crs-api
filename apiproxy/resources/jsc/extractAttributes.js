var requestBody = context.getVariable("request.content");
requestBody = JSON.parse(requestBody);

context.setVariable('billingId', requestBody.billingId);

var type = null;
var map = {
    "DC":"DIRECT_CREDIT",
    "DM":"MANUAL_DIRECT_DEBIT",
    "DA":"AUTOMATIC_DIRECT_DEBIT",
    "DD": "DEMAND_DIRECT_DEBIT"
}

if(requestBody.autoPayProduct) {
    type = map[requestBody.autoPayProduct.type];
} else if(requestBody.directCreditProduct) {
    type = map[requestBody.directCreditProduct.type];
} else if(requestBody.manualPayProduct) {
    type = map[requestBody.manualPayProduct.type];
}


context.setVariable('type', type);
context.setVariable('verb', context.getVariable("request.verb"));